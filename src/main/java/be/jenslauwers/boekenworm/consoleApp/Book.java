package be.jenslauwers.boekenworm.consoleApp;



import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name="Books")
        @NamedQuery(name = "Book.archive", query = "SELECT b FROM Book b WHERE b.isArchived=true order by b.date")
        @NamedQuery(name = "Book.getAll", query = "SELECT b FROM Book b WHERE b.isArchived=false ")
        @NamedQuery(name = "Book.getAllTitle", query = "SELECT b FROM Book b WHERE b.isArchived=false ORDER BY b.title")
        @NamedQuery(name = "Book.getAllAuthor", query = "SELECT b FROM Book b WHERE b.isArchived=false ORDER BY b.author,b.title")
        @NamedQuery(name = "Book.getAllArchivedTitle", query = "SELECT b FROM Book b WHERE b.isArchived=true ORDER BY b.title")
public class Book {
    @Id
    @Column(name = "Isbn")

    private String isbn;

    @Column(name="Title")


    String title;

    @Column(name="Author")
    String author;

    @Column(name="RevisionNr")
    int revisionNr;

    @Column(name="Type")
    String type;

    @Column(name="Fiction")
    boolean fiction;

    @Column(name = "isArchived")
    boolean isArchived;

    @Column(name ="dateArchived")
    LocalDate date;



    public Book() {
    }


    public Book(String isbn, String title, String author, int revisionNr, String type, boolean fiction) {


        setIsbn(isbn);
        setTitle(title);
        setAuthor(author);
        setRevisionNr(revisionNr);
        setType(type);
        setFiction(fiction);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if (title == null || title.length() == 0) {
            throw new RuntimeException("Missing required Book properties");
        }
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        if (author == null || author.length() == 0) {
            throw new RuntimeException("Missing required Book properties");
        }
        this.author = author;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        if (isbn == null || isbn.length() == 0) {
            throw new RuntimeException("Missing required Book properties");
        }
        this.isbn = isbn;
    }

    public int getRevisionNr() {
        return revisionNr;
    }

    public void setRevisionNr(int revisionNr) {
        if (revisionNr <= 0) {
            throw new RuntimeException("Invalid amount number, must be bigger than zero");
        }
        this.revisionNr = revisionNr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type == null) {
            throw new RuntimeException("Missing required Book properties");
        }
        this.type = type;
    }

    public boolean isFiction() {
        return fiction;
    }

    public void setFiction(boolean fiction) {
        this.fiction = fiction;
    }

    @Override
    public String toString() {
        return "Books{" +
                "ISBN= " + isbn +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", revisionNr='" + revisionNr + '\'' +
                ", type='" + type + '\'' +
                ", fiction='" + fiction + '\'' +
                ", readDate='" + date + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return isbn == book.isbn &&
                Objects.equals(title,book.title) &&
                Objects.equals(author,book.author) &&
                Objects.equals(revisionNr, book.revisionNr) &&
                Objects.equals(type,book.type) &&
                Objects.equals(fiction,book.fiction);

    }
    @Override
    public int hashCode() {
        return Objects.hash(isbn,title,author,revisionNr,type,fiction);
    }
}

