package be.jenslauwers.boekenworm.consoleApp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public abstract class DAOBase {
    static final String persistenceUnitName = "course";
    static EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        try {
            if (emf == null) emf = Persistence.createEntityManagerFactory(persistenceUnitName);

            return emf.createEntityManager();
        } catch (Exception e) {
            throw new RuntimeException("Could not open connection to DB.", e);
        }
    }

    public static void closeEntityManagerFactory() {
        if (emf != null) {
            emf.close();
            emf = null;
        }
    }
}