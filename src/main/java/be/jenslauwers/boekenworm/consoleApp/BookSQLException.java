package be.jenslauwers.boekenworm.consoleApp;

public class BookSQLException extends Exception{
    public BookSQLException() {
    }

    public BookSQLException(String message) {
        super(message);
    }

    public BookSQLException(String message, Throwable cause) {
        super(message, cause);
    }

    public BookSQLException(Throwable cause) {
        super(cause);
    }
}
