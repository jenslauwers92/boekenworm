package be.jenslauwers.boekenworm.consoleApp;

import java.util.List;

public interface BookDAOInterface {
    List<Book> getAllBooks() throws BookSQLException;
    List<Book> getAllBooksTitle() throws BookSQLException;
    List<Book> getAllBooksAuthor() throws BookSQLException;
    List<Book> getAllArchivedBooks() throws BookSQLException;
    List<Book> getAllArchivedBooksTitle() throws BookSQLException;
    Book getNextBook() throws BookSQLException;
    Book getUnderBook() throws BookSQLException;
    Book getRandomBook() throws BookSQLException;
    Book getBookByIsbn(String isbn) throws BookSQLException;
    Book addBook(Book book) throws BookSQLException;
    Book updateBook(Book book) throws BookSQLException;
    void removeBook(Book book) throws BookSQLException;
}
