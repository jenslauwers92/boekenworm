package be.jenslauwers.boekenworm.consoleApp.tools;

public final class StringTool {

    private StringTool(){}


    public static String leftString(String str, int length) {
        if (str == null)
            return null;
        if (length <= 0)
            return "";
        if (length > str.length())
            return str;
        return str.substring(0, length);
    }


    public static String cleanISBN(String isbn) {
        if (isbn == null)
            return null;
        isbn = isbn.replace(" ", "");
        return isbn.replace("-", "");
    }


   /** public static boolean isISBN(int isbn) {
        // null is not a valid ISBN value
        if (isbn == null)
            return false;
        // cleaning for validation
        isbn = cleanISBN(isbn);
        // does it only contain numbers? if not, return false
        if (!isbn.matches("\\d++"))
            return false;
        // is it 13 characters long?
        return isbn.length() == 13;
    }
**/


    public static String readableISBN(String isbn) {
        if (isbn == null)
            return null;
        StringBuilder sb = new StringBuilder(isbn);
        int[] spacesList = {3, 6, 11, 15};
        for (int i : spacesList) {
            if (sb.length() > i)
                sb.insert(i, "-");
        }
        return sb.toString();
    }
}


