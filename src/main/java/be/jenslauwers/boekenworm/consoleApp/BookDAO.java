package be.jenslauwers.boekenworm.consoleApp;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.time.LocalDate;
import java.util.List;
import java.util.Random;

public class BookDAO extends DAOBase implements BookDAOInterface{
    @Override
    public Book getBookByIsbn(String isbn) throws BookSQLException {
        EntityManager em = getEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            Book book = em.find(Book.class, isbn);
            tx.commit();
            return book;
        } catch (Exception e) {
            throw new BookSQLException(e);
        } finally {
            if (em != null)
                em.close();
        }
    }

    @Override
    public void removeBook(Book book) throws BookSQLException {
        EntityManager em = getEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();

            em.remove(em.contains(book)? book : em.merge(book));
            tx.commit();
        } catch (Exception e) {
            throw new BookSQLException(e);
        } finally {
            if (em != null)
                em.close();
        }
    }

    @Override
    public Book addBook(Book book) throws BookSQLException {
        EntityManager em = getEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(book);
            tx.commit();
            return book;
        } catch (Exception e) {
            throw new BookSQLException(e);
        } finally {
            if (em != null)
                em.close();
        }
    }

    @Override
    public Book updateBook(Book book) throws BookSQLException {
        EntityManager em = getEntityManager();
        try {
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            book.isArchived=true;
            book.date= LocalDate.now();
            em.merge(book);
            tx.commit();
            return book;
        } catch (Exception e) {
            throw new BookSQLException(e);
        } finally {
            if (em != null)
                em.close();
        }
    }



    private List<Book> getBookByNamedQuery(String queryName) throws BookSQLException {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Book> q = em.createNamedQuery(queryName, Book.class);
            return q.getResultList();
        } catch (Exception e) {
            throw new BookSQLException(e);
        } finally {
            if (em != null)
                em.close();
        }
    }
    @Override
    public List<Book> getAllBooks() throws BookSQLException {
        return getBookByNamedQuery("Book.getAll");
    }

    @Override
    public List<Book> getAllArchivedBooks() throws BookSQLException {
        return getBookByNamedQuery("Book.archive");
    }

    @Override
    public Book getNextBook() throws BookSQLException {
        EntityManager em = getEntityManager();

        List<Book> bookList;
        try {
            bookList = em.createQuery("SELECT b From Book b " +
                    "WHERE b.isArchived = false order by b.isbn asc").getResultList();
            return bookList.get(0);
        } catch (Exception e) {
            throw new BookSQLException(e);
        }
    }

    @Override
    public Book getUnderBook() throws BookSQLException {
        EntityManager em = getEntityManager();

        List<Book> bookList;
        try {
            bookList = em.createQuery("SELECT b From Book b " +
                    "WHERE b.isArchived = false order by b.isbn desc ").getResultList();
            return bookList.get(0);
        } catch (Exception e) {
            throw new BookSQLException(e);
        }
    }

    @Override
    public Book getRandomBook() throws BookSQLException {
        EntityManager em = getEntityManager();
        List<Book> bookList= getAllBooks() ;
        Random random = new Random();
        int id = random.nextInt(bookList.size());

        try {
            bookList = em.createQuery("SELECT b From Book b " +
                    "WHERE b.isArchived = false order by b.isbn asc ").getResultList();
            return bookList.get(id);
        } catch (Exception e) {
            throw new BookSQLException(e);
        }
    }

    @Override
    public List<Book> getAllBooksTitle() throws BookSQLException {
        return getBookByNamedQuery("Book.getAllTitle");
    }

    @Override
    public List<Book> getAllBooksAuthor() throws BookSQLException {
        return getBookByNamedQuery("Book.getAllAuthor");
    }

    @Override
    public List<Book> getAllArchivedBooksTitle() throws BookSQLException {
        return getBookByNamedQuery("Book.getAllArchivedTitle");
    }

}

