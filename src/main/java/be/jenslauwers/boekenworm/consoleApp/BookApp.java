package be.jenslauwers.boekenworm.consoleApp;

import be.jenslauwers.boekenworm.consoleApp.tools.ConsoleTool;



public class BookApp {
    final ConsoleTool consoleInput = new ConsoleTool();
    final BookDAO bookDAO = new BookDAO();

    public static void main(String[] args) throws BookSQLException {
        BookApp app = new BookApp();
        app.startMenu();
    }

    public void startMenu() throws BookSQLException {
        System.out.println();

        int choice;// = -1;
        do {
            System.out.println();
            System.out.println("Book main menu:");
            System.out.println("1. Add book to list to read");
            System.out.println("2. Show all books to read");
            System.out.println("3. Show next book to read");
            System.out.println("4. Mark book as read");
            System.out.println("5. Show all archived Books");
            System.out.println("6. Delete book");
            System.out.println("0. Exit");
            choice = consoleInput.askUserInputInteger("Your choice: ", 0, 6);
            switch (choice) {
                case 1:
                    addBook();
                    break;
                case 2:
                    showAllMenu();
                    break;
                case 3:
                    nextMenu();
                    break;
                case 4:
                    updateBook();
                    break;
                case 5:
                    archiveMenu();
                    break;
                case 6:
                    removeBook();
                    break;
            }
        } while (choice != 0);
        System.out.println("Bye!");
        DAOBase.closeEntityManagerFactory();
    }

    void addBook() {
        System.out.println();
        System.out.println("Add book:");
        String isbn = consoleInput.askUserInputString("isbn", 13);
        String title = consoleInput.askUserInputString("title: ", 2);
        String author = consoleInput.askUserInputString("author: ", 2);
        int revisionNr = consoleInput.askUserInputInteger("revisionNr: ");
        String type = consoleInput.askUserInputString("type: ", 2);
        boolean fiction = consoleInput.askYesOrNo("Fiction?");


        try {
            Book book = new Book(isbn, title, author, revisionNr, type, fiction);
            System.out.println(book);
            if (consoleInput.askYesOrNo("Do you want to save this book? (y/n): ")) {
                bookDAO.addBook(book);
                System.out.println("Book saved!");
            }
        } catch (BookSQLException ase) {
            System.out.println("Oops something went wrong: " + ase.getMessage());
        }
    }

    void showAllBooks() {
        try {
            bookDAO.getAllBooks().forEach(System.out::println);
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    void showAllArchivedBooks() {
        try {
            bookDAO.getAllArchivedBooks().forEach(System.out::println);
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    void removeBook() {
        String isbn = consoleInput.askUserInputString("Give the isbn of the book to remove: ", 13);

        try {
            Book book = bookDAO.getBookByIsbn(isbn);
            System.out.println(book);
            if (consoleInput.askYesOrNo("Do you want to delete this book? (y/n): ")) {
                bookDAO.removeBook(book);
                System.out.println("Book deleted!");
            }
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    void updateBook() {
        String isbn = consoleInput.askUserInputString("Give the isbn of the book to remove: ", 13);

        try {
            Book book = bookDAO.getBookByIsbn(isbn);
            System.out.println(book);
            if (consoleInput.askYesOrNo("Do you want to mark this book as read? (y/n): ")) {
                bookDAO.updateBook(book);
                System.out.println("Book marked!");
            }


        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }

    void getFirstBook() throws BookSQLException {
        System.out.println(bookDAO.getNextBook());

    }

    void getLastBook() throws BookSQLException {
        System.out.println(bookDAO.getUnderBook());
    }

    void getRandBook() throws BookSQLException {
        System.out.println(bookDAO.getRandomBook());
    }

    void nextMenu() throws BookSQLException {

        System.out.println();

        int choice;// = -1;
        do {
            System.out.println();
            System.out.println("Next book main menu:");
            System.out.println("1.Get first book on list to read");
            System.out.println("2.Get last book on list to read");
            System.out.println("3.Get random book on list to read");
            System.out.println("0. Exit");
            choice = consoleInput.askUserInputInteger("Your choice: ", 0, 3);
            switch (choice) {
                case 1:
                    getFirstBook();
                    break;
                case 2:
                    getLastBook();
                    break;
                case 3:
                    getRandBook();
                    break;

            }
        } while (choice != 0);
    }

    void showAllMenu() throws BookSQLException {
        System.out.println();

        int choice;// = -1;
        do {
            System.out.println();
            System.out.println("Show books menu:");
            System.out.println("1. Show all books ordered by isbn");
            System.out.println("2. Show all books ordered by title");
            System.out.println("3. Show all books ordered by author");
            System.out.println("0. Exit");
            choice = consoleInput.askUserInputInteger("Your choice: ", 0, 3);
            switch (choice) {
                case 1:
                    showAllBooks();
                    break;
                case 2:
                    showAllBooksTitle();
                    break;
                case 3:
                    showAllBooksAuthor();
                    break;


            }
        } while (choice != 0);
    }
    void showAllBooksTitle() {
        try {
            bookDAO.getAllBooksTitle().forEach(System.out::println);
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }
    void showAllBooksAuthor() {
        try {
            bookDAO.getAllBooksAuthor().forEach(System.out::println);
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }
    void archiveMenu() throws BookSQLException {
        System.out.println();

        int choice;// = -1;
        do {
            System.out.println();
            System.out.println("Show archived books menu:");
            System.out.println("1. Show all  archived books ordered by readDate");
            System.out.println("2. Show all books ordered by title");
            System.out.println("0. Exit");
            choice = consoleInput.askUserInputInteger("Your choice: ", 0, 2);
            switch (choice) {
                case 1:
                    showAllArchivedBooks();
                    break;
                case 2:
                    showAllArchivedBooksTitle();
                    break;
            }
        } while (choice != 0);
    }
    void showAllArchivedBooksTitle() {
        try {
            bookDAO.getAllArchivedBooksTitle().forEach(System.out::println);
        } catch (BookSQLException e) {
            System.out.println("Oops something went wrong: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
