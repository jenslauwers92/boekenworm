package be.jensLauwers.boekenworm;

import be.jenslauwers.boekenworm.consoleApp.Book;
import be.jenslauwers.boekenworm.consoleApp.BookSQLException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BookTest {
    Book book;

    @BeforeEach
    void init(){
        book = new Book("1234567890007","Test","Test",907,"Type",true);
    }

    @Test
    void setIsbn(){
        assertThrows(BookSQLException.class,()->book.setIsbn(""));
        book.setIsbn("1234567890008");
        assertEquals("1234567890008",book.getIsbn());

    }
    @Test
    void setTitle(){
        assertThrows(RuntimeException.class,()->book.setTitle(""));
        assertThrows(RuntimeException.class,()->book.setTitle(null));
        book.setTitle("boekje");
        assertEquals("boekje",book.getTitle());

    }
    @Test
    void setAuthor(){
        assertThrows(RuntimeException.class,()->book.setAuthor(""));
        assertThrows(RuntimeException.class,()->book.setAuthor(null));
        book.setAuthor("boekje");
        assertEquals("boekje",book.getAuthor());

    }
    @Test
    void setRevisionNr(){
        assertThrows(RuntimeException.class,()->book.setRevisionNr(-1));

        book.setRevisionNr(123);
        assertEquals(123,book.getRevisionNr());

    }
    @Test
    void setType(){
        assertThrows(RuntimeException.class,()->book.setType(null));

        book.setType("boekje");
        assertEquals("boekje",book.getType());

    }
}
