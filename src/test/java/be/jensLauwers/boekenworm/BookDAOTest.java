package be.jensLauwers.boekenworm;

import be.jenslauwers.boekenworm.consoleApp.Book;
import be.jenslauwers.boekenworm.consoleApp.BookDAO;
import be.jenslauwers.boekenworm.consoleApp.BookSQLException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BookDAOTest {
    static BookDAO bookDAO = new BookDAO();

@Test
   void testGetBookByIsbn() throws BookSQLException {
    Book book = bookDAO.getBookByIsbn("1234567890060");

    assertEquals(book.getAuthor(),bookDAO.getBookByIsbn("1234567890060").getAuthor());
    assertEquals(book.getTitle(),bookDAO.getBookByIsbn("1234567890060").getTitle());
}
@Test
    void testAddBook() throws BookSQLException {
    Book book = new Book();
    book.setIsbn("1234567890068");
    book.setTitle("hallo");
    book.setAuthor("wereld");
    book.setRevisionNr(123);
    book.setType("comedy");
    book.setFiction(true);
    bookDAO.addBook(book);
    assertEquals("1234567890069",book.getIsbn());
    assertEquals("hallo",book.getTitle());
    assertEquals("wereld",book.getAuthor());
    assertEquals(123,book.getRevisionNr());
    assertEquals("comedy",book.getType());
    assertEquals(true,book.isFiction());
}
@Test
    void testUpdateBook() throws BookSQLException {
    Book updateBook = new Book( "1234567890000", "halloo", "world", 100,"typetje",true);
    updateBook.setIsbn("1234567890008");
    updateBook.setTitle("hello");
    bookDAO.updateBook(updateBook);
    Book testBook = bookDAO.getBookByIsbn("1234567890008");
    assertEquals("hello", testBook.getTitle());

}
}
